'use strict'

const path = require('path')
const { app, ipcMain, Menu } = require('electron')

const Window = require('./Window')
const DataStore = require('./DataStore')

require('electron-reload')(__dirname)

// create a new  store name 
const dataSource = new DataStore({ name: 'Data Source' })

function main() {
  // Menu.setApplicationMenu(false);
  // main window
  let mainWindow = new Window({
    file: path.join('renderer', './Home/index.html')
  })
  // importSetting window
  let importSetting


  // initialize with default
  mainWindow.once('show', () => {
    mainWindow.maximize();
    mainWindow.webContents.send('importSetting', dataSource.importSetting);
  })

  // create import setting window
  ipcMain.on('add-import-setting-window', () => {
    // if importSetting does not already exist
    if (!importSetting) {
      // create a new import setting window
      importSetting = new Window({
        file: path.join('renderer', './Import/Import.html'),
        width: 800,
        height: 700,
        // close with the main window
        parent: mainWindow
      })
      importSetting.maximize();
      // cleanup
      importSetting.on('closed', () => {
        importSetting = null
      })
    }
  })

  // create export setting window
  ipcMain.on('add-export-setting-window', () => {
    // if importSetting does not already exist
    if (!importSetting) {
      // create a new import setting window
      importSetting = new Window({
        file: path.join('renderer', './Export/Export.html'),
        width: 800,
        height: 700,
        // close with the main window
        parent: mainWindow
      })
      importSetting.maximize();
      // cleanup
      importSetting.on('closed', () => {
        importSetting = null
      })
    }
  })

  // add import config setting 
  ipcMain.on('add-import-task-config', (event, data) => {
    const updatedData = dataSource.addImport(data).importSetting;
    console.log("Updated Data:", updatedData);
    mainWindow.send('importSetting', updatedData)
    importSetting.destroy();
  })

  // delete-import setting from main window
  ipcMain.on('delete-import-config', (event, importData) => {
    const updatedData = dataSource.deleteImport(importData).importSetting

    mainWindow.send('importSetting', updatedData)
  })
}

app.on('ready', main)

app.on('window-all-closed', function () {
  app.quit()
})
