'use strict'

const { ipcRenderer } = require('electron')
const sql = require("mssql");
const $ = require('jquery');
const anaplanAuthentication = require('../Anaplan/anaplan-auth');
const anaplanRequest = require('../Anaplan/anaplan-request');

//connect to MSSQL
async function getDatabaseConnection(config) {
    return new sql.ConnectionPool(config).connect()
        .then(pool => {
            console.log('Connected successfully to MSSQL')
            return pool;
        })
        .catch(err => console.log('Database Connection Failed!', err))
}


//get data from tables    
async function getTableData(query, config) {
    return new Promise(async (resolve, reject) => {
        try {
            const pool = await getDatabaseConnection(config);
            const result = await pool.request().query(query);
            resolve(result.recordset);
        } catch (err) {
            reject(err.message);
        }
    });

}

//insert exported data into database
async function insertExportData(config, query) {
    try {
        const pool = await getDatabaseConnection(config);
        const result = await pool.request().query(query);
        // pool.end();
        console.log("Executed Export:", result);
        return result.recordset;
    } catch (err) {
        return Promise.reject(err.message);
    }
}

// create import window button
document.getElementById('configuresettings').addEventListener('click', () => {
    ipcRenderer.send('add-import-setting-window')
})

// create export window button
document.getElementById('exportconfiguresettings').addEventListener('click', () => {
    ipcRenderer.send('add-export-setting-window')
})



// on receive state
ipcRenderer.on('importSetting', (event, data) => {
    console.log("Main Function:", data);
    const elementList = document.getElementById('stateData')

    // create html string
    const dataItems = data.reduce((html, data) => {
        if (data != null) {
            html += `<tr class="item"><td>${data.hostname ? data.hostname : ""}</td><td>${data.port ? data.port : ""}</td><td>${data.database ? data.database : ""}</td><td>${data.serverusername ? data.serverusername : ""}</td><td>${data.serverpassword ? data.serverpassword : ""}</td><td>${data.instance ? data.instance : ""}</td><td>${data.anaplanusername ? data.anaplanusername : ""}</td><td>${data.anaplanpassword ? data.anaplanpassword : ""}</td><td>${data.Workspace ? data.Workspace : ""}</td><td>${data.model ? data.model : ""}</td><td>${data.tableSelectedName ? data.tableSelectedName : ""}</td><td>${data.columnSelectedName ? data.columnSelectedName : ""}</td><td>${data.fileID ? data.fileID : ""}</td><td>${data.importID ? data.importID : ""}</td><td><button class="import-task-btn" onclick=${data.type == "import" ? "onClickImportAction(this)" : "onClickExportAction(this)"} data=${JSON.stringify(data)}>${data.type == "import" ? "Import" : "Export"}</button></td></tr>`
            return html;
        }
        else
            return html
    }, '<tr><th>Hostname</th><th>Port</th><th>Database</th><th>Server Username</th><th>Server User Password</th><th>Instance Name</th><th>Anaplan Username</th><th>Anaplan User Password</th><th>Workspace</th><th>Model</th><th>Table Selected</th><th>Column Selected</th><th>File ID</th><th>Import ID/Export ID</th><th>Action</th></tr>')

    // set list html to the  items
    elementList.innerHTML = dataItems
})

//import action handler
function onClickImportAction(event) {
    $(".loader-container").css("display", "block");
    let data = JSON.parse(event.getAttribute('data'));

    let config = {
        user: data.serverusername,
        password: data.serverpassword,
        server: data.hostname,
        database: data.database,
        options: {
            trustedconnection: true,
            enableArithAbort: true,
            instancename: data.instance
        },
        port: parseInt(data.port)
    }
    let anaplanConfig = {
        workspace_id: data.Workspace,
        model_id: data.model,
        file_id: data.fileID,
        import_id: data.importID
    }
    console.log(data);

    let query = data.columnSelectedName == "All" ? 'SELECT * FROM ' + data.tableSelectedName : 'SELECT ' + data.columnSelectedName + ' FROM ' + data.tableSelectedName;
    getTableData(query, config).then(tableData => {
        console.log(tableData);
        let csv = tableData.map((row) => Object.values(row));
        csv.unshift(Object.keys(tableData[0]));
        let csvData = csv.join("\n");

        let token = "";
        anaplanAuthentication
            .authentication(
                data.anaplanusername,
                data.anaplanpassword
            )
            .then((anaplanToken) => {
                token = anaplanToken;
                return anaplanRequest.uploadFile(
                    token,
                    csvData,
                    anaplanConfig
                );
            })
            .then((uploadStatus) =>
                anaplanRequest.completeUploadFile(token, anaplanConfig)
            )
            .then((completeFileStatus) =>
                anaplanRequest.triggerImportID(token, anaplanConfig)
            )
            .then((triggerStatus) => {
                $(".loader-container").css("display", "none");
                console.log("Status:", triggerStatus)
                alert("Successfully imported to anaplan.");
            })
            .catch((error) => {
                $(".loader-container").css("display", "none");
                console.log("Error:", error);
                alert("Something went wrong while importing to anaplan.");
            });
    }).catch((error) => {
        $(".loader-container").css("display", "none");
        console.log("Error:", error);
        alert("Something went wrong while importing to anaplan.");
    });
}


//export action handler
function onClickExportAction(event) {

    $(".loader-container").css("display", "block");
    let data = JSON.parse(event.getAttribute('data'));

    let config = {
        user: data.serverusername,
        password: data.serverpassword,
        server: data.hostname,
        database: data.database,
        options: {
            trustedconnection: true,
            enableArithAbort: true,
            instancename: data.instance
        },
        port: parseInt(data.port)
    }
    let anaplanConfig = {
        workspace_id: data.Workspace,
        model_id: data.model,
        file_id: data.fileID,
        export_id: data.importID
    }
    console.log(data);

    var token = "";
    anaplanAuthentication
        .authentication(
            data.anaplanusername,
            data.anaplanpassword
        )
        .then((anaplanToken) => {
            token = anaplanToken;
            return anaplanRequest.triggerExportAction(
                token,
                anaplanConfig
            );
        })
        .then((exportAction) =>
            anaplanRequest.getExportActionStatus(
                token,
                exportAction,
                anaplanConfig
            )
        )
        .then((exportStatus) =>
            anaplanRequest.getFileData(token, anaplanConfig)
        )
        .then((fileData) => {
            console.log("File Data:", fileData);
            const lineRegex = /((\\\n)|[^\n])+/g;
            const datumRegex = /,?(("(\\"|.)+?")|([^",][^,]*))?/g;
            const tableData = fileData.match(lineRegex).map((row) =>
                row.match(datumRegex).map((datum) => datum.replace(/^,?"?|"$/g, "").trim()),
            );
            tableData.forEach((item) => item.splice(-1, 1));
            let query = `INSERT INTO ${data.tableSelectedName} (${data.columnSelectedName})  VALUES ${tableData.slice(1)
                .map((item) => "(" + item.map((val) => "'" + val + "'").join(",") + ")")
                .join(",")} ;`;
            console.log("Query:", query);
            insertExportData(config, query).then(result => {
                $(".loader-container").css("display", "none");
                alert("Successfully exported from anaplan.");
            })
        })
        .catch((error) => {
            $(".loader-container").css("display", "none");
            console.log("Error:", error);
            alert("Something went wrong while importing to anaplan.");
        });
}
