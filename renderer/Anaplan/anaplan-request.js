var https = require("https");
var request = require("request");
const anaplanConfigs = require("../configs/anaplan");

var anaplanRequest = {
    uploadFile: (token, data, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
                hostname: anaplanConfigs.ANAPLAN_CONFIG_BASE_URL,
                port: 443,
                path:
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/files/" +
                    anaplan.file_id,//+
                // "/chunks/0",
                method: "PUT",
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/octet-stream",
                },
            };
            let request = https.request(options, function (response) {
                console.log(`Upload File StatusCode: ${response.statusCode}`);
                resolve(response);
                response.on("data", (d) => {
                    console.log("Data:", d);
                    process.stdout.write(d);
                });
            });
            request.write(data.toString()); // write data in anaplan
            request.on("error", (e) => {
                reject(e);
                console.log("Error in data:", e);
            });
            request.end();
        });
    },
    completeUploadFile: (token, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
                method: "POST",
                url:
                    "https://" +
                    anaplanConfigs.ANAPLAN_CONFIG_BASE_URL +
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/files/" +
                    anaplan.file_id +
                    "/complete",
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    chunkCount: 1,
                }),
            };
            request(options, function (error, response) {
                if (error) throw new Error(error);
                console.log(response.body);
                resolve(response);
            });
        });
    },
    triggerImportID: (token, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
                method: "POST",
                url:
                    "https://" +
                    anaplanConfigs.ANAPLAN_CONFIG_BASE_URL +
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/imports/" +
                    anaplan.import_id +
                    "/tasks",
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    localeName: "en_US",
                }),
            };
            request(options, function (error, response) {
                if (error) throw new Error(error);
                console.log(response.body);
                resolve(response);
            });
        });
    },
    triggerExportAction: (token, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
                method: "POST",
                url:
                    "https://" +
                    anaplanConfigs.ANAPLAN_CONFIG_BASE_URL +
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/exports/" +
                    anaplan.export_id +
                    "/tasks",
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/json",
                },
                body: JSON.stringify({
                    localeName: "en_US",
                }),
            };
            request(options, function (error, response) {
                if (error) throw new Error(error);
                console.log(response.body);
                resolve(response.body);
            });
        });
    },

    getExportActionStatus: (token, task, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let exportTask = JSON.parse(task);
            var options = {
                method: "GET",
                url:
                    "https://" +
                    anaplanConfigs.ANAPLAN_CONFIG_BASE_URL +
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/exports/" +
                    anaplan.export_id +
                    "/tasks/" +
                    exportTask.task.taskId,
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/json",
                },
            };
            request(options, function (error, response) {
                if (error) reject(error);
                let status = JSON.parse(response.body);
                if (status.task.currentStep.toLowerCase().includes("complete")) {
                    console.log("Export Action Completed");
                    resolve(response.body);
                } else {
                    console.log("Waiting for export action to be completed..");
                    setTimeout(() => {
                        anaplanRequest.getExportActionStatus(token, task, anaplan);
                    }, 200);
                }
            });
        });
    },
    getFileData: (token, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
                method: "GET",
                url:
                    "https://" +
                    anaplanConfigs.ANAPLAN_CONFIG_BASE_URL +
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/files/" +
                    anaplan.file_id,
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/json",
                },
            };
            request(options, function (error, response) {
                if (error) throw new Error(error);
                resolve(response.body);
            });
        });
    },
    getFileIDList: (token, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
                method: "GET",
                url:
                    "https://" +
                    anaplanConfigs.ANAPLAN_CONFIG_BASE_URL +
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/files",
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/json",
                },
            };
            request(options, function (error, response) {
                if (error) throw new Error(error);
                resolve(response.body);
            });
        });
    },
    getImportIDList: (token, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
                method: "GET",
                url:
                    "https://" +
                    anaplanConfigs.ANAPLAN_CONFIG_BASE_URL +
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/imports",
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/json",
                },
            };
            request(options, function (error, response) {
                if (error) throw new Error(error);
                resolve(response.body);
            });
        });
    },
    getExportIDList: (token, anaplan) => {
        return new Promise(function (resolve, reject) {
            let anaplanToken = JSON.parse(token);
            let options = {
                method: "GET",
                url:
                    "https://" +
                    anaplanConfigs.ANAPLAN_CONFIG_BASE_URL +
                    "/2/0/workspaces/" +
                    anaplan.workspace_id +
                    "/models/" +
                    anaplan.model_id +
                    "/exports",
                headers: {
                    Authorization: "AnaplanAuthToken " + anaplanToken.tokenInfo.tokenValue,
                    "Content-Type": "application/json",
                },
            };
            request(options, function (error, response) {
                if (error) throw new Error(error);
                resolve(response.body);
            });
        });
    },
};

module.exports = anaplanRequest;
