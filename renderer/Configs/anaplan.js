

const anaplanConfigs = {
    ANAPLAN_CONFIG_AUTH_URL: "https://auth.anaplan.com",
    ANAPLAN_CONFIG_BASE_URL: "api.anaplan.com",
}
module.exports = anaplanConfigs;

