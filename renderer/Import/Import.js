'use strict'

const { ipcRenderer } = require('electron')
const sql = require("mssql");
const anaplanAuthentication = require('../Anaplan/anaplan-auth');
const anaplanRequest = require('../Anaplan/anaplan-request');
const $ = require('jquery');

var formData = {};
var tableAllData;
var config;
//connect to MSSQL
async function getDatabaseConnection(config) {
    return new sql.ConnectionPool(config).connect()
        .then(pool => {
            console.log('Connected successfully to MSSQL')
            return pool;
        })
        .catch(err => console.log('Database Connection Failed!', err))
}



//get list of tables    
async function getTableList(config) {
    return new Promise(async (resolve, reject) => {
        var query =
            "SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE'";
        try {
            const pool = await getDatabaseConnection(config);
            const result = await pool.request().query(query);
            resolve(result.recordset);
        } catch (err) {
            reject(err.message);
        }
    });

}
//get list of tables    
async function getColumnList(tableName, config) {
    return new Promise(async (resolve, reject) => {
        var query =
            "SELECT TOP 10 * FROM " +
            tableName;
        try {
            const pool = await getDatabaseConnection(config);
            const result = await pool.request().query(query);
            resolve(result.recordset);
        } catch (err) {
            reject(err.message);
        }
    });

}


// fetch table data button
document.getElementById('fetch-table-data').addEventListener('click', () => {
    $(".loader-container").css("display", "block");

    let hostname = document.getElementById("hostname").value;
    let port = document.getElementById("port").value;
    let database = document.getElementById("database").value;
    let serverusername = document.getElementById("serverusername").value;
    let serverpassword = document.getElementById("serverpassword").value;
    let instance = document.getElementById("instance").value;

    formData.hostname = hostname;
    formData.port = port;
    formData.database = database;
    formData.serverusername = serverusername;
    formData.serverpassword = serverpassword;
    formData.instance = instance;

    config = {
        user: formData.serverusername,
        password: formData.serverpassword,
        server: formData.hostname,
        database: formData.database,
        options: {
            trustedconnection: true,
            enableArithAbort: true,
            instancename: formData.instance
        },
        port: parseInt(formData.port)
    }
    getTableList(config).then(tableList => {
        // get the element
        const elementList = document.getElementById('selectTable')
        // create html string
        const dataItems = tableList.reduce((html, data) => {
            html += '<option value=' + data.TABLE_NAME + '>' + data.TABLE_NAME + '</option>'
            return html
        }, '')
        // set list html to the  items
        elementList.innerHTML = dataItems
        $(".loader-container").css("display", "none");
        $(".fetch-table-container").css("display", "none");
    }).catch(error => {
        $(".loader-container").css("display", "none");
        alert("Something went wrong.")
        console.log("Error:", error)
    });

});

var tableValue;
function onTableSelect() {

    $(".loader-container").css("display", "block");

    tableValue = document.getElementById("selectTable").value;
    getColumnList(tableValue, config).then(tableData => {
        tableAllData = tableData;
        console.log(tableData);
        // get the element
        const elementList = document.getElementById('selectColumn')
        // create html string
        const dataItems = Object.keys(tableData[0]).reduce((html, data) => {
            html += '<option value=' + data + '>' + data + '</option>'
            return html
        }, "<option value='All'>All</option>")

        // set list html to the  items
        elementList.innerHTML = dataItems
        $(".loader-container").css("display", "none");

    }).catch(error => {
        $(".loader-container").css("display", "none");
        alert("Something went wrong.")
        console.log("Error:", error)
    });
}

// fetch import data button
document.getElementById('fetch-import-data').addEventListener('click', () => {

    $(".loader-container").css("display", "block");

    let anaplanusername = document.getElementById("anaplanusername").value;
    let anaplanpassword = document.getElementById("anaplanpassword").value;
    let Workspace = document.getElementById("Workspace").value;
    let model = document.getElementById("model").value;

    formData.anaplanusername = anaplanusername;
    formData.anaplanpassword = anaplanpassword;
    formData.Workspace = Workspace;
    formData.model = model;

    console.log("Import:", formData);

    let anaplan = {
        workspace_id: formData.Workspace,
        model_id: formData.model,
    }
    anaplanAuthentication.authentication(formData.anaplanusername, formData.anaplanpassword)
        // anaplanAuthentication.authentication("arif.siddiqui@polestarllp.com", "Arif@5409")
        .then(token => {
            anaplanRequest.getFileIDList(token, anaplan).then(fileIDList => {
                var fileData = JSON.parse(fileIDList);
                console.log("File ID:", fileData.files);
                const elementList = document.getElementById('selectFileID')
                // create html string
                const dataItems = fileData.files.reduce((html, data) => {
                    html += '<option value=' + data.id + '>' + data.name + '</option>'
                    return html
                }, '')
                // set list html to the  items
                elementList.innerHTML = dataItems

                anaplanRequest.getImportIDList(token, anaplan).then(importIDlist => {
                    var importData = JSON.parse(importIDlist);
                    console.log(" ID:", importData.imports);
                    const elementList = document.getElementById('selectImportID')
                    // create html string
                    const dataItems = importData.imports.reduce((html, data) => {
                        html += '<option value=' + data.id + '>' + data.name + '</option>'
                        return html
                    }, '')
                    // set list html to the  items
                    elementList.innerHTML = dataItems
                })
                $(".fetch-Anaplan-container").css("display", "none");
                $(".loader-container").css("display", "none");
            })
        }).catch(error => {
            $(".loader-container").css("display", "none");
            alert("Something went wrong.")
            console.log("Error:", error)
        });
})

var selectedValues;
var isAll;
function onColumnSelect() {
    selectedValues = [];
    $("#selectColumn :selected").each(function () {
        if ($(this).val() == 'All') {
            isAll = true;
            displayAllColumns();
            $(".preview-data").css("display", "none");
        }
        else {
            isAll = false;
            selectedValues.push($(this).val());
            displaySelectedColumns(selectedValues);
            $(".preview-data").css("display", "none");
        }
    });
}

function displayAllColumns() {
    if (tableAllData) {
        const elementList = document.getElementById('table-data');
        elementList.innerHTML = '';

        // create table header string
        let tableHeader = Object.keys(tableAllData[0]).reduce((header, data) => {
            header += `<th>${data}</th>`;
            return header
        }, '')
        tableHeader += '</tr>'

        //create table row data
        let tableRowData;
        const dataItems = tableAllData.map((rowData, index) => {
            let tableHtml = '<tr>';
            let template = Object.keys(rowData).reduce((html, data) => {
                html += `<td>${tableAllData[index][data]}</td>`;
                return html
            }, '')
            tableHtml += template;
            tableHtml += '</tr>';
            tableRowData += tableHtml;
        })

        // set list html to the  items
        elementList.innerHTML = tableHeader + tableRowData;
        tableRowData, tableHeader = '';
    }
}

function displaySelectedColumns(selectedValues) {
    let temp = tableAllData;
    let newTable = [];
    temp.map((data, index) => {
        let obj = {};
        selectedValues.map((name) => {
            obj[name] = data[name];
            return obj;
        });
        newTable.push(obj);
    });

    console.log("New Table:", newTable);

    if (newTable) {
        const elementList = document.getElementById('table-data');
        elementList.innerHTML = '';

        // create table header string
        let tableHeader = Object.keys(newTable[0]).reduce((header, data) => {
            header += `<th>${data}</th>`;
            return header
        }, '')
        tableHeader += '</tr>'

        //create table row data
        let tableRowData;
        const dataItems = newTable.map((rowData, index) => {
            let tableHtml = '<tr>';
            let template = Object.keys(rowData).reduce((html, data) => {
                html += `<td>${newTable[index][data]}</td>`;
                return html
            }, '')
            tableHtml += template;
            tableHtml += '</tr>';
            tableRowData += tableHtml;
        })

        // set list html to the  items
        elementList.innerHTML = tableHeader + tableRowData;
        tableRowData, tableHeader = '';
    }
}

// submit button function
document.getElementById('import-submit').addEventListener('click', () => {

    let fileID = document.getElementById("selectFileID").value;
    let importID = document.getElementById("selectImportID").value;

    if (Object.entries(formData).length == 0 || !tableValue || !selectedValues || !fileID || !importID) {
        alert("Please fill the form")
    }
    else {
        let columnData = isAll ? "All" : selectedValues.join(",");

        formData.tableSelectedName = tableValue;
        formData.columnSelectedName = columnData;
        formData.fileID = fileID;
        formData.importID = importID;
        formData.type = "import";
        ipcRenderer.send('add-import-task-config', formData);

    }
})
