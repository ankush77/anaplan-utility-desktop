'use strict'

const Store = require('electron-store')

class DataStore extends Store {
  constructor(settings) {
    super(settings)
    // initialize  or empty array
    this.importSetting = this.get('importSetting') || []
  }

  saveImport() {
    // save  to JSON file
    this.set('importSetting', this.importSetting)

    // returning 'this' allows method chaining
    return this
  }

  getImport() {
    // set object's  in JSON file
    this.importSetting = this.get('importSetting') || []

    return this
  }

  addImport(importSetting) {
    // merge the existing importSettings with the new importSetting
    this.importSetting = [...this.importSetting, importSetting]

    return this.saveImport()
  }

  deleteImport(importSetting) {
    // filter out the target 
    this.importSetting = this.importSetting.filter(t => t !== importSetting)

    return this.saveImport()
  }
}

module.exports = DataStore
